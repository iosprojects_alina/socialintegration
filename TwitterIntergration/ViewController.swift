//
//  ViewController.swift
//  TwitterIntergration
//
//  Created by Alina Chernenko on 5/5/16.
//  Copyright © 2016 dimalina. All rights reserved.
//

import UIKit
import Social

class ViewController: UIViewController {

    @IBOutlet weak var tweetTextView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureTweetTextView()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func showShareAction(sender: AnyObject) {
        
        if self.tweetTextView.isFirstResponder(){
            self.tweetTextView.resignFirstResponder()
        }
        let actionController = UIAlertController(title: "", message: "Tweet your note", preferredStyle: UIAlertControllerStyle.Alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Default, handler:nil

        
        )
        let tweetAction = UIAlertAction(title: "Tweet", style: .Default, handler: {
            (action:UIAlertAction!) in
            if SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter){
                
                let twitterVC = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
                //tweet out tweet
                if self.tweetTextView.text.characters.count < 140 {
                    twitterVC.setInitialText(self.tweetTextView.text)
                }
                else{
                    let index = self.tweetTextView.text.startIndex.advancedBy(140)
                    let shortText = self.tweetTextView.text.substringToIndex(index)
                    twitterVC.setInitialText(shortText)
                }
                
                self.presentViewController(twitterVC, animated: true, completion: nil)
            }
            else {
                //Raise some objection
                self.showAlertMessage("Please sign in to Twitter")
            }
        })
        
        let facebookAction = UIAlertAction(title: "Post to Facebook", style: .Default, handler: {
            (action:UIAlertAction!) in
            if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook){
                
                let facebookVC = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
                //tweet out tweet
                //if self.tweetTextView.text.characters.count < 140 {
                  //  twitterVC.setInitialText(self.tweetTextView.text)
                //}
               //else{
                    //let index = self.tweetTextView.text.startIndex.advancedBy(140)
                    //let shortText = self.tweetTextView.text.substringToIndex(index)
                    facebookVC.setInitialText(self.tweetTextView.text)
               //}
                
                self.presentViewController(facebookVC, animated: true, completion: nil)
         }
           else {
//                //Raise some objection
               self.showAlertMessage("Please sign in to Facebook")
           }
        })
        
        let moreAction = UIAlertAction(title: "More", style: .Default, handler: {
            (action:UIAlertAction!) in
            
            let moreVC = UIActivityViewController(activityItems: [self.tweetTextView.text], applicationActivities: nil)
            self.presentViewController(moreVC, animated: true, completion: nil)
            
            
        })
        
        
        actionController.addAction(tweetAction)
        actionController.addAction(facebookAction)
        actionController.addAction(moreAction)
        actionController.addAction(cancelAction)
       
        
        self.presentViewController(actionController, animated: true, completion: nil)
        
    }

    func configureTweetTextView(){
    
        self.tweetTextView.layer.backgroundColor = (UIColor(colorLiteralRed: 1.0, green: 1.0, blue: 0.9, alpha: 1.0)).CGColor
        
        self.tweetTextView.layer.cornerRadius = 10.0
        self.tweetTextView.layer.borderColor = (UIColor(white: 0, alpha: 0.5)).CGColor
        self.tweetTextView.layer.borderWidth = 2.0
        
    }
    
    func showAlertMessage (myMessage: String){
        let alertController = UIAlertController(title: "SocialShare", message: myMessage, preferredStyle: UIAlertControllerStyle.Alert)
        
        alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
        
        
    }
}

